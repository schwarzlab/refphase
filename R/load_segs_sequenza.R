#' Load sequenza segmentations into a GRangesList object
#'
#' chromosome      start.pos       end.pos Bf      N.BAF   sd.BAF  depth.ratio     N.ratio sd.ratio        CNt     A       B       LPP
#' chr1    69428   16490694        0.0841846801425084      286     0.103775540599156       0.636847194003657       6937    0.243546328089087       1       1       0       -7.89928473808901
#' chr1    16536007        16924560        0.299607517828638       28      0.120183914399654       0.706248127291556       231     0.259588786209733       1       1       0       -8.19005641908264
#' chr1    17237472        115768346       0.088985339103264       934     0.0946521097025763      0.590808298013719       21094   0.239757328276689       1       1       0       -7.02336867642786
#' chr1    116389941       120449395       0.499284746758341       49      0.0692800009917362      1.04125726362545        1187    0.406650058621383       2       1       1       -6.73347549842622
#'
#' @param samples Sample information containing sample_id and segmentation path
#' @return GRangesList object containing input segmentations with copy numbers
#' as metadata (columns = major, minor)
load_segs_sequenza <- function(samples) {
  segs_list <- list()

  for (i in seq_len(nrow(samples))) {
    sample_id <- samples$sample_id[[i]]
    segments <- utils::read.delim(samples[i, "segmentation"])
    segs_list[[sample_id]] <- GenomicRanges::makeGRangesFromDataFrame(segments,
      keep.extra.columns = TRUE,
      ignore.strand = TRUE,
      seqnames.field = "chromosome",
      start.field = "start.pos",
      end.field = "end.pos",
      starts.in.df.are.0based = FALSE
    )
    mcols(segs_list[[sample_id]])$cn_major <- mcols(segs_list[[sample_id]])$A
    mcols(segs_list[[sample_id]])$cn_minor <- mcols(segs_list[[sample_id]])$B

    mcols(segs_list[[sample_id]]) <- mcols(segs_list[[sample_id]])[, c("cn_major", "cn_minor")]
  }

  segs_list
}
