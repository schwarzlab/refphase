#' Sequenza input format
#'
#' Read BAF and logR values from seqz files that are generated for sequenza.
#' These files are seqz format, a tab-delimited and gzipped file format with the
#' following columns:
#'
#' * chromosome
#' * position
#' * base.ref
#' * depth.normal
#' * depth.tumor
#' * depth.ratio
#' * Af
#' * Bf
#' * zygosity.normal
#' * GC.percent
#' * good.reads
#' * AB.normal
#' * AB.tumor
#' * tumor.strand
#'
#' example file:
#' $ tabix CB1001-11-D01-X1.gz "chr1:10,000,000-20,000,000;" | head
#' chr1    10007515        C       15      5       0.333   1.0     0       hom     32      5       C       .       0
#' chr1    10007516        T       15      5       0.333   1.0     0       hom     32      5       T       .       0
#' chr1    10007517        T       16      5       0.312   1.0     0       hom     32      5       T       .       0
#' chr1    10007518        C       16      5       0.312   1.0     0       hom     32      5       C       .       0
#' chr1    10007519        A       15      5       0.333   1.0     0       hom     32      5       A       .       0
#' chr1    10007520        C       17      5       0.294   1.0     0       hom     32      5       C       .       0
#'
#' @param samples data.frame with one row per sample, including a "snps" column
#'   with snp data file locations
#' @param het_only a logical scalar. If TRUE filter out all positions that are
#'   not heterozygous
#' @param normalize_logr If TRUE, normalize counts that are used for logr
#'   calculation by the total number of reads in that sample.
#' @return a list of GPos objects, one per input sample
load_snps_seqz <- function(samples, het_only = FALSE, normalize_logr = TRUE) {
  all_snps <- list()

  input_files <- samples$snps
  names(input_files) <- samples$sample_id

  for (samp in names(input_files)) {
    input_file <- input_files[[samp]]

    # All data that is available in a .seqz file. We only need a subset of it
    # but this is a useful reference in case we need more info at a later date
    #
    # col_types = "ciciidddcddccc"
    # col_names = c("chromosome", "position", "base.ref", "depth.normal",
    #               "depth.tumor", "depth.ratio", "Af", "Bf", "zygosity.normal",
    #               "GC.percent", "good.reads", "AB.normal", "AB.tumor",
    #               "tumor.strand")
    # Skip unnecessary columns
    col_types <- "cicdddddc--c--"
    col_names <- c("chromosome", "position", "base.ref", "depth.normal", "depth.tumor", "depth.ratio", "Af", "Bf", "zygosity.normal", "AB.normal")
    seqz_data <- readr::read_tsv(input_file,
      col_types = col_types,
      progress = FALSE,
      col_names = col_names,
      skip = 1
    )

    # Only keep heterozygous positions
    if (isTRUE(het_only)) {
      germline_het <- seqz_data$zygosity.normal == "het"
      seqz_data <- seqz_data[germline_het, ]
    }

    chrom <- seqz_data$chromosome
    pos <- seqz_data$position

    # Bf is always < Af, so we need to check the AB.normal to see if we need to flip it.
    # This is probably not the perfect thing to do. Look into this topic more.
    flip_baf <- nchar(seqz_data$AB.normal) > 0 & seqz_data$base.ref != substr(seqz_data$AB.normal, 1, 1)
    baf <- seqz_data$Bf
    baf[flip_baf] <- seqz_data$Af[flip_baf]

    # Normalize depth per position by total depth in the sample before calculating logr
    if (isTRUE(normalize_logr)) {
      sum_normal <- sum(seqz_data$depth.normal, na.rm = TRUE)
      sum_tumor <- sum(seqz_data$depth.tumor, na.rm = TRUE)
      norm_normal <- seqz_data$depth.normal / sum_normal
      norm_tumor <- seqz_data$depth.tumor / sum_tumor
      logr <- log2(norm_tumor) - log2(norm_normal)
    } else {
      logr <- log2(seqz_data$depth.ratio)
    }

    gpos <- GPos(
      seqnames = Rle(chrom),
      pos = pos,
      baf = baf,
      logr = logr,
      germline_zygosity = as.factor(seqz_data$zygosity.normal)
    )

    # Mask non-hz SNPs with NA
    mcols(gpos)$baf[mcols(gpos)$germline_zygosity != "het"] <- NA

    all_snps[[samp]] <- gpos
  }

  all_snps
}
