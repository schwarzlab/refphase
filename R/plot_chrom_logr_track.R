#' Plot a logr track for a single chromosome and sample
#'
#' @param data_sample SNP data for the sample we are plotting (including logr)
#' @param segs_sample Segmentation for the sample we are plotting
#' @param xlim min and max genomic positions to plot with 1 as the start of the
#'   chromosome
#' @param ytrim trim this percent of logr values, to reduce the impact of
#'   having a few outliers
#' @param max_values downsample the positions to a maximum of this number of
#'   positions
#' @param cn_event_calls Sample table with copy number event calls
#' @param samplename Sample name
#' @param arm_threshold genomic position of p-q arm
plot_chrom_logr_track <- function(data_sample, segs_sample = NA, xlim, ytrim = 0.001, max_values, cn_event_calls, samplename, arm_threshold, cn_event_type = "relative") {
  par(mar = c(0.1, 5, 0.1, 1))

  if (!missing(max_values)) {
    data_sample <- data_sample[sample.int(nrow(data_sample), min(max_values, length(data_sample)))]
  }

  logr <- GenomicRanges::mcols(data_sample)$logr
  pos <- GenomicRanges::start(data_sample)

  # Trim away the top and bottom % of values when plotting
  if (ytrim > 0) {
    ylim <- quantile(logr, probs = c(ytrim, 1 - ytrim))
  } else {
    ylim <- c(min(logr), max(logr))
  }

  plot(pos, logr, ylab = "logR", xaxt = "n", col = grDevices::adjustcolor("darkgrey", 0.2), pch = 16, cex = 0.75, ylim = c(min(c(-2, ylim[[1]])), max(c(2, ylim[[2]]))), xlim = xlim)

  # Skip everything related to the segmentation if we don't supply one
  if (length(segs_sample) == 1 && is.na(segs_sample)) {
    return
  }

  abline(v = unique(c(GenomicRanges::start(segs_sample), GenomicRanges::end(segs_sample))), lty = 2, col = "black") # dashed vertical lines at ends of segs for chromosome.pdf
  abline(v = arm_threshold, lty = 1, lwd = 2, col = "black") # separating the p and q arm

  if (cn_event_type == "relative") {
    abline(h = unique(cn_event_calls[cn_event_calls$sample_id == samplename, "amplification_logRthreshold"]), lty = 2, col = "darkred") # line for logR amplifications
    abline(h = unique(cn_event_calls[cn_event_calls$sample_id == samplename, "gain_logRthreshold"]), lty = 2, col = "red") # line for logR gains
    abline(h = unique(cn_event_calls[cn_event_calls$sample_id == samplename, "loss_logRthreshold"]), lty = 2, col = "blue") # line for logR losses
  }

  # Fit a loess regression line
  fit <- stats::loess(logr ~ pos, span = 0.05)
  points(x = pos, y = stats::predict(fit, pos), col = "black", pch = ".")

  # Plot per-segment loess smoothed lines
  min_points <- 1
  for (seg_idx in seq_len(length(segs_sample))) {
    seg <- segs_sample[seg_idx]
    data_seg <- IRanges::subsetByOverlaps(data_sample, seg)

    segments(
      x0 = GenomicRanges::start(seg), y0 = mean(GenomicRanges::mcols(data_seg)$logr),
      x1 = GenomicRanges::end(seg), col = "black", lwd = 1, lend = 1
    )
  }
}
