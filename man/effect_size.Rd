% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/reference_phase.R
\name{effect_size}
\alias{effect_size}
\title{Return the effect size (Cohen's d)}
\usage{
effect_size(baf, phasing)
}
\arguments{
\item{baf}{a vector of BAF values}

\item{phasing}{a character vector the same length as baf, indicating phasing
information ("a", "b" or "none")}
}
\value{
a numeric value. The larger the value, the greater the effect
}
\description{
Return the effect size (Cohen's d)
}
